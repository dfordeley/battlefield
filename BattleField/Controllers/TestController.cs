﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BattleField.Controllers
{
    public class TestController : BaseController
    {
        // GET api/values/5
        public string Get(int id)
        {
            var z = Logic.TransformerService.Get(id);

            var a  = Logic.TransformerService.GetOverallScore(id);
            
            return a.ToString();
        }
    }
}
