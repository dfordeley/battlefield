﻿using BattleField.Logic.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace BattleField.Controllers
{
   
    public class BaseController : ApiController
    {
        private LogicModule _module;

        /// <summary>
        /// 
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        public LogicModule Logic
        {
            get
            {
                if (_module == null)
                {
                    _module = new LogicModule();
                }
                return _module;
            }
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        public string ModelError(ModelStateDictionary modelState)
        {
            string error = "";
            foreach (var state in modelState.Values)
            {
                foreach (var msg in state.Errors)
                {
                    error += msg.ErrorMessage + "<br />";
                }
            }
            return error;
        }

    }
}
