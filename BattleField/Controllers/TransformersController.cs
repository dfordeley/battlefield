﻿using BattleField.Common;
using BattleField.Domain.Form;
using BattleField.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace BattleField.Controllers
{
    /// <summary>
    ///  
    /// </summary>
    [RoutePrefix("api/Transformers")]
    public class TransformersController : BaseController
    {
        /// <summary>
        ///1. Add New Transformer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("Create")]
        [ResponseType(typeof(TransformerModel))]
        [HttpPost]
        public IHttpActionResult Create(TransformerForm form)
        {
            try
            {
                if(!string.IsNullOrEmpty(form.Allegiance))
                {

                    form.Allegiance = form.Allegiance.ToUpper();
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelError(ModelState));
                }
                form.Name = form.Name.ToUpper();
                var check = Logic.TransformerService.CheckExist(form);
                if (check)
                {
                    return BadRequest("Transformer exists");
                }
                var resp = Logic.TransformerService.AddNew(form);

                if (resp == null)
                    return BadRequest();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }
            
        }

        /// <summary>
        ///1. Add New Transformer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [Route("CreateBulk")]
        [ResponseType(typeof(TransformerModel))]
        [HttpPost]
        public IHttpActionResult CreateBulk(List<TransformerForm> form)
        {
            try
            {
                var errormsg = "";
                var succesList = new List<TransformerModel>();
                foreach(var item in form)
                {
                    if (!string.IsNullOrEmpty(item.Allegiance))
                    {
                        item.Name =  item.Name.ToUpper();
                        item.Allegiance = item.Allegiance.ToUpper();
                    }
                    if (!ModelState.IsValid)
                    {
                         errormsg += (ModelError(ModelState)) + Util.SerializeJSON(item);
                        continue;
                    }

                    var check = Logic.TransformerService.CheckExist(item);
                    if (check)
                    {
                        errormsg += ("Transformer exists") + Util.SerializeJSON(item);
                    }
                    var resp = Logic.TransformerService.AddNew(item);
                    succesList.Add(resp);
                }
                if (string.IsNullOrEmpty(errormsg))
                    return Ok(succesList);
                return Ok("Some Items Failed" + errormsg );
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }

        /// <summary>
        ///  Retrieving the details of a registered Transformer 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Route("Get")]
        [ResponseType(typeof(TransformerModel))]
        [HttpGet]
        public IHttpActionResult Get(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return BadRequest("Invalid Id Supplied");
                }

                
                var resp = Logic.TransformerService.GetDetails(Id);

                if (resp == null)
                    return BadRequest();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }

        /// <summary>
        /// Search for Transformer details( -- A different way for solving question 2 )
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Search")]
        [ResponseType(typeof(List<TransformerModel>))]
        public IHttpActionResult Search(string name = "", int courage = 0, int endurance = 0, int firepower = 0, int intelligence = 0, int rank = 0,
            int skill = 0, int speed = 0, int strength = 0, string allegiance = "")
        {
            try
            {

                var resp = Logic.TransformerService.SearchModel(name, courage, endurance, firepower, intelligence, rank, skill, speed, strength, allegiance);

                if (resp == null)
                    return BadRequest();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
        /// <summary>
        /// Update Existing Transformer
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [ResponseType(typeof(TransformerModel))]
        public IHttpActionResult Update(TransformerForm form)
        {
            try
            {
                if (!string.IsNullOrEmpty(form.Allegiance))
                {
                    form.Allegiance = form.Allegiance.ToUpper();
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelError(ModelState));
                }
                form.Name = form.Name.ToUpper();
                var dbItem = Logic.TransformerService.Get(form.Id);
                if (dbItem == null)
                {
                    return BadRequest("Transformer doesn't exist");
                }

                var resp = Logic.TransformerService.Update(dbItem, form, "Name,Allegiance,Strength,Intelligence,Speed,Endurance,Rank,Courage,Firepower,Skill");

                if (resp == null)
                    return BadRequest();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
        /// <summary>
        /// Remove Transformer
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpDelete]
        [ResponseType(typeof(int))]
        [Route("Delete")]
        public IHttpActionResult Delete(int Id)
        {
            try
            {
                if(Id < 1)
                {
                    return BadRequest("Invalid Id supplied");
                }

                var dbItem = Logic.TransformerService.Get(Id);
                if (dbItem == null)
                {
                    return BadRequest("Transformer doesn't exist");
                }

                var resp = Logic.TransformerService.Delete(dbItem);

                if (resp == 0)
                    return BadRequest("An Error Occured");
                return Ok("Delete successful");
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }

        /// <summary>
        ///  Get the list of all autobots by name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAutobots")]
        [ResponseType(typeof(List<TransformerModel>))]
        public IHttpActionResult GetAutobots()
        {
            try
            {
                var resp = Logic.TransformerService.Search(allegiance: "AUTOBOT").ApplySort("Name").Select(Logic.TransformerService.FactoryModule.Transformers.CreateModel);
                if (resp == null)
                    return BadRequest("An Error Occured");
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
        /// <summary>
        /// Get the list of all Decepticons ordered by name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDecepticons")]
        [ResponseType(typeof(List<TransformerModel>))]
        public IHttpActionResult GetDecepticons()
        {
            try
            {
                var resp = Logic.TransformerService.Search(allegiance: "DECEPTICON").ApplySort("Name").Select(Logic.TransformerService.FactoryModule.Transformers.CreateModel);
                if (resp == null)
                    return BadRequest("An Error Occured");
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
        /// <summary>
        /// Get Overall Score
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("OverallScore")]
        [ResponseType(typeof(OverallScoreModel))]
        public IHttpActionResult OverallScore(int Id)
        {
            try
            {
                if (Id < 1)
                {
                    return BadRequest("Invalid Id Supplied");
                }

                var resp = Logic.TransformerService.Get(Id);

                if (resp == null)
                    return BadRequest("Transformer doesn't exist");
                var score = Logic.TransformerService.GetOverallScore(Id);
                return Ok(score);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
        /// <summary>
        /// War Simulation
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SimulationResult")]
        [ResponseType(typeof(SimulationResult))]
        public IHttpActionResult SimulationResult()
        {
            try
            {
                var resp = Logic.TransformerService.SimulateBattle();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                FileLog.Error(ex);
                return BadRequest("An Error Occured");
            }

        }
    }
}
