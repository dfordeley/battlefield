﻿using BattleField.Logic.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Logic.Module
{
    public class LogicModule
    {
        private TransformerService _transformer;

        public TransformerService TransformerService { get { if (_transformer == null) { _transformer = new TransformerService(); } return _transformer; } }
    }
}
