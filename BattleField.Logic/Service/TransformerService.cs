﻿using BattleField.Common;
using BattleField.Domain.Entity;
using BattleField.Domain.Form;
using BattleField.Domain.Model;
using HashTalk.Core.Logic.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Logic.Service
{
   
    public class TransformerService : BaseService<Transformer, TransformerModel, TransformerForm, int>
    {
        /// <summary>
        /// Insert New Transformer record 
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        /// 
        public TransformerModel AddNew(TransformerForm form)
        {
                var newTransformer = Insert(form);
                return newTransformer;
        }


        /// <summary>
        /// Get Details of a transformer 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public TransformerModel GetDetails(int Id)
        {
            return Create(Get(Id));
        }

        /// <summary>
        /// Search Transfromer
        /// </summary>
        /// <param name="name"></param>
        /// <param name="courage"></param>
        /// <param name="endurance"></param>
        /// <param name="firepower"></param>
        /// <param name="intelligence"></param>
        /// <param name="rank"></param>
        /// <param name="skill"></param>
        /// <param name="speed"></param>
        /// <param name="strength"></param>
        /// <param name="allegiance"></param>
        /// <returns></returns>
        public IQueryable<Transformer> Search(string name = "", int courage = 0, int endurance = 0, int firepower = 0, int intelligence = 0, int rank = 0,
            int skill = 0, int speed = 0, int strength = 0, string allegiance ="")
        {
            return DataModule.Transformers.Search( name , courage , endurance , firepower, intelligence , rank , skill , speed , strength, allegiance);
        }
       

       /// <summary>
       /// Search Transformer, return a model
       /// </summary>
       /// <param name="name"></param>
       /// <param name="code"></param>
       /// <param name="corporateId"></param>
       /// <returns></returns>
        public IEnumerable<TransformerModel> SearchModel(string name = "", int courage = 0, int endurance = 0, int firepower = 0, int intelligence = 0, int rank = 0,
            int skill = 0, int speed = 0, int strength = 0, string allegiance = "")
        {
            return DataModule.Transformers.Search(name, courage, endurance, firepower, intelligence, rank, skill, speed, strength, allegiance)
                .Select(FactoryModule.Transformers.CreateModel);
        }


        /// <summary>
        /// Create Model From entity using the factory
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TransformerModel Create(Transformer entity)
        {
            return FactoryModule.Transformers.CreateModel(entity);
        }

        /// <summary>
        /// Create Model From Form using the factory
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public TransformerModel Create(TransformerForm form)
        {
            return FactoryModule.Transformers.CreateModel(form);
        }

        /// <summary>
        /// Create Entity From Model using the factory
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Transformer Create(TransformerModel model)
        {
            return FactoryModule.Transformers.CreateEntity(model);
        }

       /// <summary>
       /// Check if Item Exist
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
        public bool CheckExist(TransformerForm form)
        {
            var model = Create(form);
            return DataModule.Transformers.RankExists(model);
        }

        /// <summary>
        /// Delete Entity -- Actually sets the record status to deleted
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Transformer entity)
        {
            return DataModule.Transformers.Delete(entity);
        }

        /// <summary>
        /// Get Entity with Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Transformer Get(int id)
        {
            return DataModule.Transformers.Get(id);
        }

        /// <summary>
        /// Insert record into DB
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TransformerModel Insert(TransformerForm form)
        {
            
            var entity = FactoryModule.Transformers.CreateEntity(form);
            entity.RecordStatus = Domain.Enum.RecordStatus.Active;
            DataModule.Transformers.Insert(entity);
            return FactoryModule.Transformers.CreateModel(entity);
        }

        /// <summary>
        /// Path Model
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Transformer Patch(Transformer entity, TransformerModel model, string fields)
        {
            return FactoryModule.Transformers.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update record
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public TransformerModel Update(Transformer entity, TransformerForm form = null, string fields = "")
        {
            var model = Create(form);
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Transformers.CreateModel(DataModule.Transformers.Update(entity));
        }

      /// <summary>
      /// Get Overall score of Transformer
      /// </summary>
      /// <param name="TransformerId"></param>
      /// <returns></returns>

        public OverallScoreModel GetOverallScore(int TransformerId)
        {
            return DataModule.Transformers.OverallScore(TransformerId);
        }

        #region Battle
        public SimulationResult SimulateBattle()
        {
            //Retrieve all Autobots and decepticons, sort be rank
            var autobots = Search(allegiance: "AUTOBOT");
            var decepticon = Search(allegiance: "DECEPTICON");

            //Check if both  Optimus and Predaking exist -- 
            if(autobots.Where(x=>x.Name == "OPTIMUS" || x.Name == "PREDAKING").Count()>0 && decepticon.Where(y => y.Name == "OPTIMUS" || y.Name == "DECEPTICON").Count()>0)
            {
                //No Victor or survivor
                return new SimulationResult();
            }

            var result = new SimulationResult();
            result.AutobotSurvivals = new List<TransformerModel>();
            result.AutobotVictors = new List<TransformerModel>();
            result.DecepticonSurvivals = new List<TransformerModel>();
            result.DecepticonVictors = new List<TransformerModel>();
            //Since there are 10 ranks, I am going to go through each round, check if there are transformers ready to battle on each side
            //There can only be one transformer for each side per rank
            for (int i = 1; i < 11; i ++)
            {
                var autobotTransformer = autobots.Where(x => x.Rank == i).FirstOrDefault();
                var decepticonTransformer = decepticon.Where(x => x.Rank == i).FirstOrDefault();

                //If both sides doesn't have a transformer for this rank, move on
                if(autobotTransformer == null && decepticonTransformer == null)
                {
                    continue;
                }
                // if autobot is null and decepticon isn't
                else if(autobotTransformer == null && decepticonTransformer != null)
                {
                    //Mark the decepticon as survivor  
                    result.DecepticonSurvivals.Add(Create(autobotTransformer));
                }
                //if decepticon is null and autobot isn't
                else if(autobotTransformer != null && decepticonTransformer == null)
                {
                    //Mark the autobot as survivor  
                    result.AutobotSurvivals.Add(Create(autobotTransformer));
                }
                //Battle
                else
                {
                    //If either is named Optimus or Predaking
                    if(autobotTransformer.Name == "OPTIMUS" || autobotTransformer.Name == "PREDAKING")
                    {
                        //Mark autobot as victor
                        result.AutobotVictors.Add(Create(autobotTransformer));
                        continue;
                    }
                    else if(decepticonTransformer.Name == "OPTIMUS" || decepticonTransformer.Name == "PREDAKING")
                    {
                        //Mark decepticon as victor
                        result.DecepticonVictors.Add(Create(decepticonTransformer));
                        continue;
                    }

                    /*
                         If Transformer A exceeds Transformer B in strength by 3 or more and Transformer B has less than 5 courage, the battle is won by Transformer A (Transformer B ran away)
                         ● If Transformer A’s skill rating exceeds Transformer B's rating by 5 or more, Transformer A wins the fight. 
                         ● Otherwise, the victor is whomever of Transformer A and B has the higher overall rating. 

                    */
                    //If Transformer A exceeds Transformer B in strength by 3 or more and Transformer B has less than 5 courage, the battle is won by Transformer A(Transformer B ran a
                    if (autobotTransformer.Strength - decepticonTransformer.Strength >= 3 && decepticonTransformer.Courage < 5)
                    {
                        //Mark autobot as victor
                        result.AutobotVictors.Add(Create(autobotTransformer));
                        continue;
                    }
                    else if (decepticonTransformer.Strength - autobotTransformer.Strength >= 3 && autobotTransformer.Courage < 5)
                    {
                        //Mark decpticon as victor
                        result.DecepticonVictors.Add(Create(decepticonTransformer));
                        continue;
                    }

                    //● If Transformer A’s skill rating exceeds Transformer B's rating by 5 or more, Transformer A wins the fight. 
                    if (autobotTransformer.Skill - decepticonTransformer.Skill >= 5)
                    {
                        //Mark autobot as victor
                        result.AutobotVictors.Add(Create(autobotTransformer));
                        continue;
                    }
                    else if (decepticonTransformer.Skill - autobotTransformer.Skill >= 5)
                    {
                        //Mark decpticon as victor
                        result.DecepticonVictors.Add(Create(decepticonTransformer));
                        continue;
                    }
                    if(GetOverallScore(autobotTransformer.Id).OverallScore > GetOverallScore(decepticonTransformer.Id).OverallScore)
                    {
                        //Mark autobot as victor
                        result.AutobotVictors.Add(Create(autobotTransformer));
                        continue;
                    }
                    if (GetOverallScore(decepticonTransformer.Id).OverallScore > GetOverallScore(autobotTransformer.Id).OverallScore)
                    {
                        //Mark decpticon as victor
                        result.DecepticonVictors.Add(Create(decepticonTransformer));
                        continue;
                    }
                }
            }
            return result;
        }
        #endregion
    }
}
