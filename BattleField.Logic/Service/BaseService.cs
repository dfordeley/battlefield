using System.Linq;
using System.Collections.Generic;

using BattleField.Domain.Module;
using BattleField.Domain.Entity;
using BattleField.Domain.Model;
using BattleField.Domain.Form;
using BattleField.Data.Module;
using BattleField.Data.DB;
using BattleField.Logic.Module;
using BattleField.Data.Repository;

namespace HashTalk.Core.Logic.Service
{

    public class BaseService<TEntity, TModel, TForm, TKey>
        where TEntity : EntityBase<TKey>
        where TModel : ModelBase<TKey>
        where TForm : FormBase<TKey>
    {
        private DataModule _service;
        private BattleFieldDbContext _context;

       
        /// <summary>
        /// DB Service Wrapper for repositories
        /// </summary>

        //_transformerRepositor.

        public DataModule DataModule
        {
            get
            {
                if (_service == null)
                {
                    if(_context == null)
                    {
                        _context = new BattleFieldDbContext();
                    } 
                    _service = new DataModule(_context);
                }
                return _service;
            }
        }


       

        private LogicModule _module;

        /// <summary>
        /// Logic Wrapper
        /// </summary>
        public LogicModule Logic
        {
            get
            {
                if (_module == null)
                {
                    _module = new LogicModule();
                }
                return _module;
            }
        }

        private FactoryModule _factory;

        /// <summary>
        /// 
        /// </summary>
        public FactoryModule FactoryModule
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new FactoryModule();
                }
                return _factory;
            }
        }
        
    }


}