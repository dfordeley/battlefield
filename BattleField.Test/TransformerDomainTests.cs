﻿using BattleField.Domain.Entity;
using BattleField.Domain.Form;
using BattleField.Domain.Model;
using BattleField.Domain.Module;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BattleField.Test
{
    public class TransformerDomainTests
    {
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void  AddFormToList_ShouldWork()
        {

            TransformerForm form = new TransformerForm
            {
                Allegiance = "AUTOBOT",
                Courage = 2,
                Endurance = 3,
                Firepower = 4,
                Intelligence = 2,
                Name = "AGA",
                Rank = 3,
                Skill = 3,
                Speed = 8,
                Strength = 3

            };
            List<TransformerForm> formList = new List<TransformerForm>();

            formList.Add(form);

            Assert.True(formList.Count == 1);
            Assert.Contains<TransformerForm>(form, formList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="allegiance"></param>
        /// <param name="courage"></param>
        /// <param name="endurance"></param>
        /// <param name="firepower"></param>
        /// <param name="intelligence"></param>
        /// <param name="name"></param>
        /// <param name="rank"></param>
        /// <param name="skill"></param>
        /// <param name="speed"></param>
        /// <param name="strength"></param>
        [Theory]
        [InlineData("AUTOBOT", 11, 1,6,74,"",4,8,9,4)]
        public void AddPersonToPeopleList_ShouldFail(string allegiance, int courage, int endurance, int firepower, int intelligence, string name, int rank, int skill, int speed, int strength)
        {
            TransformerForm form = new TransformerForm { Allegiance = allegiance, Courage = courage, Endurance = endurance, Firepower =firepower, Intelligence = intelligence,
                                                        Name = name, Rank = rank, Skill=skill,Speed=speed, Strength= strength};
            List<TransformerForm> formList = new List<TransformerForm>();

            Assert.False(formList.Count == 1);

            //.Throws<ValidationException>("Courage", () => List<>))
        }
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ConvertFormToModel_ShouldWork()
        {
            TransformerForm form = new TransformerForm
            {
                Allegiance = "AUTOBOT",
                Courage = 2,
                Endurance = 3,
                Firepower = 4,
                Intelligence = 2,
                Name = "AGA",
                Rank = 3,
                Skill =5,
                Speed =4,
                Strength =3

            };
            var model = FactoryModule.Transformers.CreateModel(form);

            Assert.True(model != null);
            List<TransformerModel> modeList = new List<TransformerModel>();

            Assert.Contains<TransformerModel>(model, modeList);
        }
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ConvertFormToEntity_ShouldWork()
        {
            TransformerForm form = new TransformerForm
            {
                Allegiance = "AUTOBOT",
                Courage = 2,
                Endurance = 3,
                Firepower = 4,
                Intelligence = 2,
                Name = "AGA",
                Rank = 3,
                Skill = 5,
                Speed = 4,
                Strength = 3

            };
            var entity = FactoryModule.Transformers.CreateEntity(form);

            Assert.True(entity != null);
            List<Transformer> entityList = new List<Transformer>();

            Assert.Contains<Transformer>(entity, entityList);
        }
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ConvertFormToEntity_ShouldNotWork()
        {
            TransformerForm form = new TransformerForm
            {
                Allegiance = "AUTOBOT",
                Courage = 2,
                Endurance = 3,
                Firepower = 4,
                Intelligence = 2,
                Name = "AGA",
                Rank = 3,
                Skill = 15,
                Speed = 14,
                Strength = 3

            };
            var entity = FactoryModule.Transformers.CreateEntity(form);

            Assert.True(entity == null);
            List<Transformer> entityList = new List<Transformer>();

            Assert.DoesNotContain<Transformer>(entity, entityList);
        }
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ConvertEntityToModel_ShouldNotWork()
        {
            Transformer entity = new Transformer
            {
                Allegiance = "AUTOBOT",
                Courage = 2,
                Endurance = 3,
                Firepower = 4,
                Intelligence = 2,
                Name = "AGA",
                Rank = 3,
                Skill = 1,
                Speed = 1,
                Strength = 3,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Id =1,
                RecordStatus = Domain.Enum.RecordStatus.Active,
            };
            var model = FactoryModule.Transformers.CreateModel(entity);

            Assert.True(entity == null);
            List<Transformer> entityList = new List<Transformer>();

            Assert.Contains<Transformer>(entity, entityList);
        }
        private FactoryModule _factory;

        /// <summary>
        /// 
        /// </summary>
        public FactoryModule FactoryModule
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new FactoryModule();
                }
                return _factory;
            }
        }
    }
}
