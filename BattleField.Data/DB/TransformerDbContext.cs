using System.Data.Entity;
using System.Data.Entity.Design.PluralizationServices;
using BattleField.Common;
using BattleField.Domain;
using BattleField.Domain.Entity;

namespace BattleField.Data.DB
{
    /// <summary>
    /// Db Context
    /// </summary>
    public class BattleFieldDbContext : DbContext
    {
        public BattleFieldDbContext() : base(Settings.Connection())
        {

        }
        public DbSet<Transformer> Transformers { get; set; }
    }

}