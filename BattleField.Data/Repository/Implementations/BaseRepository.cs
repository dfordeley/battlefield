using System;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Collections.Generic;
using BattleField.Domain.Entity;
using BattleField.Domain.Model;
using BattleField.Data.DB;
using BattleField.Common;
using BattleField.Domain.Enum;

namespace BattleField.Data.Repository
{

    public class BaseRepository<TEntity, TModel, TKey> : IRepository<TEntity, TModel, TKey>
          where TEntity : EntityBase<TKey>
          where TModel : ModelBase<TKey>
    {
        public BattleFieldDbContext _context;
        public BaseRepository(BattleFieldDbContext context)
        {
            _context = context;
        }

        public virtual TEntity Update(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                _context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return obj;
            }
            catch (DbEntityValidationException e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB UPDATE: " + str, e);
                FileLog.Error(ex);
                return obj;
            }
            catch (Exception e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB UPDATE: " + str, e); 
                FileLog.Error(ex);
                return obj;
            }
        }


        public virtual int Delete(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                obj.RecordStatus = RecordStatus.Deleted;
                _context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB DELETE: " + str, e);
                FileLog.Error(ex);
                return 0;
            } 
            catch (Exception e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB DELETE: " + str, e);
                FileLog.Error(ex);
                return 0;
            }
        }

        public virtual TEntity Insert(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.UtcNow;
                obj.CreatedAt = DateTime.UtcNow;
                _context.Set<TEntity>().Add(obj);
                _context.SaveChanges();
                return obj;
            }
            catch (DbEntityValidationException e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB INSERT: " + str, e);
                FileLog.Error(ex);
                return obj;
            }
            catch (Exception e)
            {
                var str = Util.SerializeJSON(obj);
                var ex = new Exception("DB INSERT: " + str, e);
                FileLog.Error(ex);
                return obj;
            }
        }

        public virtual int DeleteFinally(TEntity obj)
        {
            try
            { 
                _context.Set<TEntity>().Remove(obj);
                return _context.SaveChanges();
            }
            catch (Exception e)
            {
                var ex = new Exception("DB DELETE", e);
                FileLog.Error(ex);
                return 0;
            }
        }

        public virtual int Count()
        {
            try { 
                return Query().Count();

            }
            catch (Exception e)
            {
                FileLog.Error(e);
                return 0;
            }
        }

        public virtual IQueryable<TEntity> Query(string includeProperties = "")
        {
            try
            {
                return All(includeProperties)
                    .Where(x => x.RecordStatus != RecordStatus.Deleted && x.RecordStatus != RecordStatus.Archive);
            } 
            catch (Exception e)
            {
                FileLog.Error(e); 
                return null;
            }
}

        public virtual IQueryable<TEntity> All(string includeProperties = "")
        {
            try
            {
                IQueryable<TEntity> query = _context.Set<TEntity>();
                if (!String.IsNullOrEmpty(includeProperties))
                {
                    foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        query = query.Include(includeProperty);
                    }
                }
                return query;
            }
            catch (Exception e)
            {
                FileLog.Error(e);
                return null;
            }


        }

        public bool Exists(TKey id)
        {
            try
            {
                var i = _context.Set<TEntity>().Find(id);
                if (i != null)
                {
                    if (i.RecordStatus == RecordStatus.Deleted || i.RecordStatus == RecordStatus.Archive)
                    {
                        i = null;
                    }
                }
                return i != null;
            }

            catch (Exception e)
            {
                FileLog.Error(e);
                return false;
            }
        }

        public TEntity Get(TKey id)
        {
            try
            {
                var i = _context.Set<TEntity>().Find(id);
                if (i != null)
                {
                    if (i.RecordStatus == RecordStatus.Deleted || i.RecordStatus == RecordStatus.Archive)
                    {
                        i = null;
                    }
                    else
                    {
                    }
                }
                return i;
            }
            catch (Exception e)
            {
                FileLog.Error(e);
                return null;
            }
        }

        public virtual TEntity DeleteUndo(TEntity obj)
        {
            try
            {
                obj.UpdatedAt = DateTime.Now;
                obj.RecordStatus = RecordStatus.Active;
                _context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return obj;
            }

            catch (Exception e)
            {
                FileLog.Error(e);
                return obj;
            }
        }

      
    }

}