﻿using BattleField.Common;
using BattleField.Data.DB;
using BattleField.Domain.Entity;
using BattleField.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Data.Repository
{
    public class TransformerRepository: BaseRepository<Transformer,TransformerModel,int>, ITransformerRepository
    {
        public TransformerRepository(BattleFieldDbContext context) : base(context)
        {
        }

        public IQueryable<Transformer> Search(string name = "", int courage = 0, int endurance = 0, int firepower =0, int intelligence = 0, int rank = 0,
            int skill = 0, int speed = 0, int strength = 0, string allegiance = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }

            if (!string.IsNullOrEmpty(allegiance))
            {
                table = table.Where(x => x.Allegiance == allegiance);
            }
            
            if (courage > 0)
            {
                table = table.Where(x => x.Courage == courage);
            }

            if (endurance > 0)
            {
                table = table.Where(x => x.Endurance == endurance);
            }
            if (firepower > 0)
            {
                table = table.Where(x => x.Firepower == firepower);
            }
            if (intelligence > 0)
            {
                table = table.Where(x => x.Intelligence == intelligence);
            }
            if (rank > 0)
            {
                table = table.Where(x => x.Rank == rank);
            }
            if (skill > 0)
            {
                table = table.Where(x => x.Skill == skill);
            }
            if (speed > 0)
            {
                table = table.Where(x => x.Speed == speed);
            }
            if (strength > 0)
            {
                table = table.Where(x => x.Strength == strength);
            }

            return table;
        }

     

        public bool ItemExists(TransformerModel model, long Id = 0)
        {
            var check = Search(model.Name, model.Courage, model.Endurance, model.Firepower, model.Intelligence, model.Rank, model.Skill, model.Speed, model.Strength, model.Allegiance);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
        public bool RankExists(TransformerModel model, long Id = 0)
        {
            var check = Search(rank:model.Rank, allegiance: model.Allegiance);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
        public OverallScoreModel OverallScore(int TranformerId)
        {
            try
            {
                var param = new SqlParameter("@Id", TranformerId);
                var a = _context.Database.SqlQuery<OverallScoreModel>("[dbo].[OverallScore] @Id", new System.Data.SqlClient.SqlParameter("@Id", TranformerId));
                return a.First();
            }
            catch (Exception ex)
            {

                FileLog.Error(ex);
                return new OverallScoreModel();
            }
            
        }
    }
}
