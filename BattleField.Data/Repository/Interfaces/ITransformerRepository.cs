﻿using BattleField.Domain.Entity;
using BattleField.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Data.Repository
{
    public interface ITransformerRepository:IRepository<Transformer,TransformerModel,int>
    {
        IQueryable<Transformer> Search(string name = "", int courage = 0, int endurance = 0, int firepower = 0, int intelligence = 0, int rank = 0,
           int skill = 0, int speed = 0, int strength = 0, string allegiance = "");
        bool ItemExists(TransformerModel model, long Id = 0);
        bool RankExists(TransformerModel model, long Id = 0);
        OverallScoreModel OverallScore(int TranformerId);
    }
}
