using System.Linq;

namespace BattleField.Data.Repository
{
    public interface IRepository<TEntity, TModel, TKey>
    {
        TEntity Get(TKey id);
        TEntity Update(TEntity obj);
        TEntity Insert(TEntity obj);
        int Delete(TEntity obj);
        TEntity DeleteUndo(TEntity obj);
        int DeleteFinally(TEntity obj);
        bool Exists(TKey id);
        int Count();
        IQueryable<TEntity> Query(string includeProperties = "");
        IQueryable<TEntity> All(string includeProperties = "");
    }
}