namespace BattleField.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Default : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transformers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50, unicode: false),
                        Strength = c.Int(nullable: false),
                        Intelligence = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                        Endurance = c.Int(nullable: false),
                        Rank = c.Int(nullable: false),
                        Courage = c.Int(nullable: false),
                        Firepower = c.Int(nullable: false),
                        Skill = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Transformers");
        }
    }
}
