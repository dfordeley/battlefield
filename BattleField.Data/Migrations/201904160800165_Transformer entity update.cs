namespace BattleField.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Transformerentityupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transformers", "Allegiance", c => c.String(maxLength: 10, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transformers", "Allegiance");
        }
    }
}
