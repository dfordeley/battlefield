namespace BattleField.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateStoredProcedure : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
            "dbo.OverallScore",
            p => new
            {
                Id = p.Int(),
            },
        body:
            @"SELECT Strength + Intelligence + Speed + Endurance + [Rank] + Courage + Firepower + Skill as OverallScore
                FROM [dbo].[Transformers]  
                    WHERE ([Id] = @Id)"
    );
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.OverallScore");
        }
    }
}
