
using BattleField.Data.DB;
using BattleField.Data.Repository;

namespace BattleField.Data.Module
{
    public class DataModule : IDataModule
    {

        private BattleFieldDbContext _context;
       
        public DataModule(BattleFieldDbContext context)
        {
            _context = context;
        }

        private TransformerRepository _transformer;
        public TransformerRepository Transformers { get { if (_transformer == null) { _transformer = new TransformerRepository(_context); } return _transformer; } }
        
    }
}