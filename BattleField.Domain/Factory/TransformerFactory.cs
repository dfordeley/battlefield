﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleField.Domain.Form;
using BattleField.Domain.Model;
using BattleField.Domain.Entity;
namespace BattleField.Domain.Factory
{
    /// <summary>
    /// Transformer Factory
    /// </summary>
    public class TransformerFactory : BaseFactory<Transformer, TransformerModel, TransformerForm, int>
    {
        
    }
}
