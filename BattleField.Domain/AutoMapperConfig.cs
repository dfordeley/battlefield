using AutoMapper;
using BattleField.Domain.Entity;
using BattleField.Domain.Form;
using BattleField.Domain.Model;

namespace BattleField.Domain
{

    public static class AutoMapperConfig
    {
        /// <summary>
        /// Registers the mappings.
        /// </summary>
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Transformer, TransformerModel>().ReverseMap();
                cfg.CreateMap<Transformer, TransformerForm>().ReverseMap();
                cfg.CreateMap<TransformerModel, TransformerForm>().ReverseMap();

            });
        }
    }

}