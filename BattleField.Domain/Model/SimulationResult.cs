﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Domain.Model
{
    public class SimulationResult
    {
        public List<TransformerModel> AutobotVictors { get; set; }
        public List<TransformerModel> AutobotSurvivals { get; set; }

        public List<TransformerModel> DecepticonVictors { get; set; }
        public List<TransformerModel> DecepticonSurvivals { get; set; }
    }
}
