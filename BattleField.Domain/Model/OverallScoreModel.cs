﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Domain.Model
{
    public class OverallScoreModel
    {
        public int OverallScore { get; set; }
    }
}
