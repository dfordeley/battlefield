﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Domain.Model
{
    public class TransformerModel:ModelBase<int>
    {
       
       
        public string Name { get; set; }

        public string Allegiance { get; set; }

        public int Strength { get; set; }
       
        public int Intelligence { get; set; }

        public int Speed { get; set; }

        public int Endurance { get; set; }

        public int Rank { get; set; }

        public int Courage { get; set; }
        
        public int Firepower { get; set; }

        public int Skill { get; set; }

    }

}
