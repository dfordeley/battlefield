using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Domain.Enum
{

    /// <summary>
    /// Status of a record in the database
    /// </summary>
    public enum RecordStatus
    {
        /// <summary>
        /// Pendinf state
        /// </summary>
        Pending,
        /// <summary>
        /// Flagged as active
        /// </summary>
        Active,
        /// <summary>
        /// Flagged as inactive
        /// </summary>
        Inactive,
        /// <summary>
        /// Soft deleted
        /// </summary>
        Deleted,
        /// <summary>
        /// Archivere can delete and archive record
        /// </summary>
        Archive
    }

}