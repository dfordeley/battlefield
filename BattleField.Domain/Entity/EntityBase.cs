using BattleField.Common;
using BattleField.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace BattleField.Domain.Entity
{

    /// <summary>
    /// 
    /// </summary>
    public class EntityBase<T> 
    {


        public EntityBase()
        {
            CreatedAt = Util.CurrentDateTime();
            UpdatedAt = Util.CurrentDateTime();
        }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RecordStatus RecordStatus { get; set; }
        /// <summary>
        /// Date Record was created
        /// </summary> 
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Last updated date
        /// </summary>
        public DateTime UpdatedAt { get; set; }
    }


}