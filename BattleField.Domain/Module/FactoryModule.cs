using System;
using System.Collections.Generic;
using BattleField.Domain.Factory;

namespace BattleField.Domain.Module
{

    /// <summary>
    /// 
    /// </summary>
    public class FactoryModule : IFactoryModule
    {


        private TransformerFactory _transfac;

        /// <summary>
        /// Transformer Factory Module
        /// </summary>
        public TransformerFactory Transformers { get { if (_transfac == null) { _transfac = new TransformerFactory(); } return _transfac; } }

    }



}