﻿using BattleField.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleField.Domain.Form
{
    public class TransformerForm:FormBase<int>
    {
        
        [MaxLength(50)]
        public string Name { get; set; }

        [StringRange(AllowableValues = new[] { "AUTOBOT" , "Autobot", "autobot", "DECEPTICON", "Decepticon","decepticon" }, ErrorMessage = "")]
        [MaxLength(10)]
        public string Allegiance { get; set; }

        [Range(1, 10, ErrorMessage = "Strength must be between 1 to 10")]
        public int Strength { get; set; }

        [Range(1, 10, ErrorMessage = "Intelligence must be between 1 to 10")]
        public int Intelligence { get; set; }

        [Range(1, 10, ErrorMessage = "Speed must be between 1 to 10")]
        public int Speed { get; set; }

        [Range(1, 10, ErrorMessage = "Endurance must be between 1 to 10")]
        public int Endurance { get; set; }

        [Range(1, 10, ErrorMessage = "Rank must be between 1 to 10")]
        public int Rank { get; set; }

        [Range(1, 10, ErrorMessage = "Courage must be between 1 to 10")]
        public int Courage { get; set; }

        [Range(1, 10, ErrorMessage = "Firepower must be between 1 to 10")]
        public int Firepower { get; set; }

        [Range(1, 10, ErrorMessage = "Skill must be between 1 to 10")]
        public int Skill { get; set; }
    }
}
