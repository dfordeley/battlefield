﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Web.Http.Routing;


namespace BattleField.Common
{
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Apply sorting to queryable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="sort">Sort fields delimited by comma</param>
        /// <returns></returns>
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, string sort)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (string.IsNullOrEmpty(sort))
            {
                return source;
            }

            // split the sort string
            var lstSort = sort.Split(',');

            //run through the sorting options and apply them - in reverse
            //order, otherwise results will come out sorted by the last
            // item in the string first!
            foreach (var sortOption in lstSort.Reverse())
            {
                // if the sort option starts with "-", we order
                // descending, ortherwise ascending

                if (sortOption.StartsWith("-"))
                {
                    source = source.OrderBy(sortOption.Remove(0, 1) + " descending");
                }
                else
                {
                    source = source.OrderBy(sortOption);
                }

            }

            return source;
        }
    }
}