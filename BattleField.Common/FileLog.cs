﻿using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.IO;

namespace BattleField.Common
{
    /// <summary>
    /// File Logger
    /// </summary>
    public class FileLog
    {
        private static string conn = Constants.LogPath;

        private static Logger logger = new LoggerConfiguration()
            .WriteTo.File(conn + "log.txt", rollingInterval: RollingInterval.Hour)
            .CreateLogger();
        public static void Error(Exception ex)
        {
            Seri("ERROR", ex.Message, ex);
        }
        private static void Seri(string level, string message, Exception ex, params object[] obj)
        {
            //if (!Settings.IsDebug) return;

            var lev = Serilog.Events.LogEventLevel.Information;
            switch (level)
            {
                case "INFO": lev = Serilog.Events.LogEventLevel.Information; break;
                case "ERROR": lev = Serilog.Events.LogEventLevel.Error; break;
                case "WARNING": lev = Serilog.Events.LogEventLevel.Warning; break;
            }
            logger.Write(lev, ex, message, obj);
        }

        private static void SeriOk(string level, string message, Exception ex, params object[] obj)
        {

            var lev = Serilog.Events.LogEventLevel.Information;
            switch (level)
            {
                case "INFO": lev = Serilog.Events.LogEventLevel.Information; break;
                case "ERROR": lev = Serilog.Events.LogEventLevel.Error; break;
                case "WARNING": lev = Serilog.Events.LogEventLevel.Warning; break;
            }
            logger.Write(lev, ex, message, obj);
        }


        public static void Write(string logMessage, string loc = "", string title = "Default")
        {
            Seri("INFO", title, new Exception(logMessage));
        }

        public static void Info(string v)
        {
            Seri("INFO", v, null);
        }

        public static void Trace(string controller, string actions, Dictionary<string, object> args)
        {
            throw new NotImplementedException();
        }

        public static void Write(object data, string v)
        {
            Seri("INFO", v, new Exception(Util.SerializeJSON(data)));
        }

        public static void Writeall(object data, string v)
        {
            SeriOk("INFO", v, new Exception(Util.SerializeJSON(data)));
        }
    }
    public static class xLog
    {
       
        public static void Error(Exception ex)
        {
            Write(ex.ToString(), "", "ERROR");
        }



        public static void Write(string logMessage, string loc = "", string title = "Default")
        {
           
            var dir = new System.IO.DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory);
            var logpath = dir.Parent;
            var m_exePath = logpath.FullName + "\\logs\\";
            var dtm = DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            if (!string.IsNullOrEmpty(loc))
            {
                m_exePath += loc + "\\";
            }
            if (!File.Exists(m_exePath + "\\" + dtm))
            {
                var k = File.Create(m_exePath + "\\" + dtm);
                k.Dispose();
            }
            try
            {
                using (StreamWriter w = new StreamWriter(m_exePath + "\\" + dtm, true))
                {
                    w.Write("\r\nLog Entry : " + title + " | ");
                    w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                    w.WriteLine("{0}", logMessage);
                    w.WriteLine("-");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Info(string v)
        {
            Write(v, "", "INFO");
        }

        public static void Trace(string controller, string actions, Dictionary<string, object> args)
        {
            throw new NotImplementedException();
        }

        public static void Write(object data, string v)
        {
            Write(Util.SerializeJSON(data), "", v);
        }
    }
}
