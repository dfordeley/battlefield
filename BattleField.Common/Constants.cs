using System;
using System.Web;

namespace BattleField.Common
{
    /// <summary>
    /// Stuffs that hardly change in the app
    /// 
    /// </summary>
    public class Constants
    {

        public const string ErrorMessage = "Sorry, your request could not be completed. Please try again in 5 minutes!";
        public static string LogPath
        {
            get
            {
                return Settings.Get("log.path", "c:/Logs/");
            }
        }
       

        public static string DBName
        {
            get
            {
                return Settings.Get("db.name", "battleFieldDb");
            }
        }
      
    }
}